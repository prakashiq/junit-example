package com.sc;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MyFirstTest {

    @Test
    public void testConcat() {
        Concatenation myConcat = new Concatenation();
        String result = myConcat.concat("Hello", "World");
        assertEquals("HelloWorld", result);
    }

    @Test
    public void testConcat2() {
        Concatenation myConcat = new Concatenation();
        String result = myConcat.concat("Hello", "World2");
        assertNotEquals("HelloWorld", result);
    }
}
