package com.sc;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SecondTest {

    @Test
    public void testEven() {
        OddCheck oddCheck = new OddCheck();

        List<Integer> evenVaribles = new ArrayList<>(
                Arrays.asList(1, 3, 7, 9, 11)
        );

        for (Integer val : evenVaribles) {
            boolean isOdd = oddCheck.checkDigit(val);

            Assert.assertFalse(isOdd);
        }

    }

    @Test
    public void testOdd() {
        OddCheck oddCheck = new OddCheck();

        List<Integer> oddVaribles = new ArrayList<>(
                Arrays.asList(2, 8, 22, 14, 16)
        );

        for (Integer val : oddVaribles) {
            boolean isOdd = oddCheck.checkDigit(val);

            Assert.assertTrue(isOdd);
        }


    }
}
